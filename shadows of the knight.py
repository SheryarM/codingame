import sys
import math

# w: width of the building.
# h: height of the building.
w, h = [int(i) for i in input().split()]
n = int(input())  # maximum number of turns before game over.
x, y = [int(i) for i in input().split()]  # initial position
h0, w0 = 0, 0

# game loop
while True:
    # the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
    bomb_dir = input()
    
    if(bomb_dir == "UL"):
        h = y ; y = math.floor((h0+y)/2)
        w = x ; x = math.floor((w0+x)/2)
    elif(bomb_dir == "U"):
        h = y ; y = math.floor((h0+y)/2)
    elif(bomb_dir == "UR"):
        h = y ; y = math.floor((h0+y)/2)
        w0 = x ; x = math.floor((x+w)/2)
    elif(bomb_dir == "L"):
        w = x ; x = math.floor((w0+x)/2)
    elif(bomb_dir == "R"):
        w0 = x ; x = math.floor((x+w)/2)
    elif(bomb_dir == "DL"):
        h0 = y ; y = math.floor((y+h)/2)
        w = x ; x = math.floor((w0+x)/2)
    elif(bomb_dir == "D"):
        h0 = y ; y = math.floor((y+h)/2)
    else:
        h0 = y ; y = math.floor((y+h)/2)
        w0 = x ; x = math.floor((w0+w-1)/2)

    print(bomb_dir,file=sys.stderr)
    print(x,file=sys.stderr) ; print(y,file=sys.stderr)
    
    # the location of the next window Batman should jump to.
    print(x, y)
