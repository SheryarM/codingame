import sys

abs_temp = {}
all_temp = []
n = int(input())  # the number of temperatures to analyse
for i in input().split():
    # t: a temperature expressed as an integer ranging from -273 to 5526
    t = int(i)
    abs_temp[t] = abs(t) # associate with the distance from 0°C

if(not n): # if no data
    print(0)
else:
    sorted(abs_temp, reverse=True)
    
    if(len(abs_temp.keys()) == 1): # if one single data
        print(list(abs_temp.keys())[0])
    else:
        min_temp = min(abs_temp.values()) # find the temperature close to 0°C
        
        for k,v in abs_temp.items():
            if(v == min_temp):
                all_temp.append(k)
        
        print("all temp key=", file=sys.stderr, end=" ")
        print(all_temp, file=sys.stderr)
        
        if(len(all_temp) == 2): # in case of [-5, 5] take 5
            print(abs(all_temp[0]))
        else:
            print(all_temp[0])
