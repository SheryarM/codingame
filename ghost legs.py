import sys
import math

def next_direction(grid,w,h,start):
    i, j = 1, start
    while(i != h-1):
        # turn right
        if(j-3 >= 0 and grid[i][j-1] == '-'):
            i += 1
            j -= 3
        # turn left
        elif(j+3 <= w-1 and grid[i][j+1] == '-'):
            i += 1
            j += 3
        # otherwise go down
        else:
            i += 1

    return j

grid = []
alphanum = {}
w, h = [int(i) for i in input().split()]
for i in range(h):
    line = input()
    grid.append(line)
    #print(line,file=sys.stderr)

# look-up table index-character
for i,c in enumerate(grid[h-1]):
    if(not c.isspace()):
        alphanum[i] = c
for i,c in enumerate(grid[0]):
    if(not c.isspace()):
        res = next_direction(grid,w,h,i)
        print(c,end="")
        print(alphanum[res])
