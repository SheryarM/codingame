import sys
from datetime import datetime

mem_lg = {} # memorize length from the node
def longuest_path(adj_matrix, node, nbr_node):
    max_length = 0
    # if no person influenced return 1
    if(adj_matrix[node].count(0) == nbr_node):
        return 1
    if(node in mem_lg):
        return mem_lg[node]
    for i in range(len(adj_matrix[node])):
        if(adj_matrix[node][i] == 1):
            max_length = max(max_length, longuest_path(adj_matrix, i, nbr_node))
    mem_lg[node] = 1+max_length
    return 1+max_length

all_influence = []
adj_matrix = []
nodes = []
zeros = []
loc = {}
ind = 0
n = int(input())  # the number of relationships of influence
for i in range(n):
    # x: a relationship of influence between two people (x influences y)
    x, y = [int(j) for j in input().split()]
    all_influence.append((x,y))

all_influence = sorted(all_influence, key=lambda tup:tup[0])
for i in range(len(all_influence)):
    print(all_influence[i], file=sys.stderr)
for i in range(len(all_influence)):
    if(all_influence[i][0] not in nodes):
        nodes.append(all_influence[i][0])
        loc[all_influence[i][0]] = ind
        ind += 1
    if(all_influence[i][1] not in nodes):
        nodes.append(all_influence[i][1])
        loc[all_influence[i][1]] = ind
        ind += 1

# create the matrix
for i in range(len(nodes)):
    for j in range(len(nodes)):
        zeros.append(0)
    adj_matrix.append(zeros)
    zeros = []

# create the adjacency matrix
for i in range(len(all_influence)):
    adj_matrix[loc[all_influence[i][0]]][loc[all_influence[i][1]]] = 1
print(adj_matrix, file=sys.stderr)

start_time = datetime.now()

max_length = 0
for i in range(len(nodes)):
    r = longuest_path(adj_matrix, node=i, nbr_node=len(adj_matrix[0]))
    max_length = max(max_length, r)
print(mem_lg, file=sys.stderr)

end_time = datetime.now()
print('algo duration: {}'.format(end_time - start_time), file=sys.stderr)

print(max_length)
