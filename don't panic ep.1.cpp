#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

template<typename Map>
void PrintMap(Map& m)
{
    cerr << "[ ";
    for (auto &item : m) {
        cerr << item.first << ":" << item.second << " ";
    }
    cerr << "]\n";
}

int main()
{
    vector<int> elevatorsF; vector<int> elevatorsP;
    int nbFloors; // number of floors
    int width; // width of the area
    int nbRounds; // maximum number of rounds
    int exitFloor; // floor on which the exit is found
    int exitPos; // position of the exit on its floor
    int nbTotalClones; // number of generated clones
    int nbAdditionalElevators; // ignore (always zero)
    int nbElevators; // number of elevators
    cin >> nbFloors >> width >> nbRounds >> exitFloor >> exitPos >> nbTotalClones >> nbAdditionalElevators >> nbElevators; cin.ignore();
    
    for (int i = 0; i < nbElevators; i++) {
        int elevatorFloor; // floor on which this elevator is found
        int elevatorPos; // position of the elevator on its floor
        cin >> elevatorFloor >> elevatorPos; cin.ignore();
        elevatorsF.push_back(elevatorFloor);
        elevatorsP.push_back(elevatorPos);
    }
    for (int i = 0; i < nbElevators; i++) {
        cerr << elevatorsP[i] << " " ;
    }
    cerr << endl;
    for (int i = 0; i < nbElevators; i++) {
        cerr << elevatorsF[i] << " " ;
    }
    cerr << endl;

    map <int,int> elevPos;
    for(int i=0; i<nbElevators; i++){
        elevPos[elevatorsF[i]] = elevatorsP[i];
    }
    PrintMap(elevPos);
    
    vector <bool> block(nbElevators+1,false);
    
    // game loop
    while (1) {
        int cloneFloor; // floor of the leading clone
        int clonePos; // position of the leading clone on its floor
        string direction; // direction of the leading clone: LEFT or RIGHT
        cin >> cloneFloor >> clonePos >> direction; cin.ignore();
        cerr << "clone de tête=" << cloneFloor << " " << clonePos << " " << direction << endl;
        
        if(clonePos > elevPos[cloneFloor] and direction == "RIGHT" and !block[cloneFloor]){
            cout << "BLOCK" << endl;
            block[cloneFloor] = true;
        }
        else if(clonePos < elevPos[cloneFloor] and direction == "LEFT" and !block[cloneFloor]){
            cout << "BLOCK" << endl;
            block[cloneFloor] = true;
        }
        else if(clonePos < exitPos and direction == "LEFT" and cloneFloor == exitFloor and !block[cloneFloor]){
            cout << "BLOCK" << endl;
            block[cloneFloor] = true;
        }
        else{
            cout << "WAIT" << endl;
        }
    }
}
///////* trouver la position de la tête, le faire tourner vers la position de l'ascenceur puis bloquer *///////
