import sys
import math

all_defib = []  # contains all defib infos
data = []
dis = {}        # associate a distance to each defib from the user
lon_defib, lat_defib = 0, 0
lon = float(input().replace(',', '.', 1))  # longitude of the user
lat = float(input().replace(',', '.', 1))  # latitude of the user
n = int(input())
for i in range(n):
    defib = input()
    all_defib.append(defib)

for i in range(len(all_defib)):
    data = all_defib[i].split(sep=';')
    lon_defib = float(data[-2].replace(',', '.', 1)) # extract the longitude
    lat_defib = float(data[-1].replace(',', '.', 1)) # extract the latitude
    x = ((lon_defib*math.pi/180)-(lon*math.pi/180))*math.cos(((lat*math.pi/180)+(lat_defib*math.pi/180))*0.5)
    y = (lat_defib*math.pi/180)-(lat*math.pi/180)
    d = math.sqrt((x**2)+(y**2))*6371
    dis[i] = d
min_dis = min(dis, key=lambda k: dis[k])  # find the shortest defib from the user 
print(all_defib[min_dis].split(';')[1])
