import sys
import math

def count_ones(loc,w,h):
    i, j = 1, 1
    for k in range(h):
        for l in range(w):
            # if there are eight '1' surronding one '0' then print the location
            if(loc[i][j]==0 and loc[i-1][j-1]==1 and loc[i-1][j]==1 and loc[i-1][j+1]==1 \
            and loc[i][j-1]==1 and loc[i][j+1]==1 and loc[i+1][j-1]==1 and loc[i+1][j]==1 and loc[i+1][j+1]==1):
                print(j-1,i-1)
            j += 1
        i += 1
        j = 1

w = int(input())
h = int(input())
line = [] ; loc = [] # the grid

# add virtualy extra '1' over the actual grid given by Codingame
line = [1]*(w+2)
loc.append(line)
line = [1]
for i in range(h):
    for j in input().split():
        line.append(int(j))
    line.append(1)
    loc.append(line)
    line = [1]
line = []
line = [1]*(w+2)
loc.append(line)

# find the treasure location
count_ones(loc,w,h)
