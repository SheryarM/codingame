#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int find_number(vector<string> arr, int index, vector<vector<string>> seg_list, int choice);

int main()
{
    vector<string> arr;

    string line1;
    getline(cin, line1); arr.push_back(line1);
    string line2;
    getline(cin, line2); arr.push_back(line2);
    string line3;
    getline(cin, line3); arr.push_back(line3);
    
    vector<vector<string>> seg_list;
    vector<string> seg_val0;
    vector<string> seg_val1;
    vector<string> seg_val2;
    vector<string> seg_val3;
    vector<string> seg_val4;
    vector<string> seg_val5;
    vector<string> seg_val6;
    vector<string> seg_val7;
    vector<string> seg_val8;
    vector<string> seg_val9;
    seg_val0 = {{' ','_',' '},{'|',' ','|'},{'|','_','|'}}; /* 0 represented in 7-seg */
    seg_val1 = {{' ',' ',' '},{' ',' ','|'},{' ',' ','|'}};
    seg_val2 = {{' ','_',' '},{' ','_','|'},{'|','_',' '}};
    seg_val3 = {{' ','_',' '},{' ','_','|'},{' ','_','|'}};
    seg_val4 = {{' ',' ',' '},{'|','_','|'},{' ',' ','|'}};
    seg_val5 = {{' ','_',' '},{'|','_',' '},{' ','_','|'}};
    seg_val6 = {{' ','_',' '},{'|','_',' '},{'|','_','|'}};
    seg_val7 = {{' ','_',' '},{' ',' ','|'},{' ',' ','|'}};
    seg_val8 = {{' ','_',' '},{'|','_','|'},{'|','_','|'}};
    seg_val9 = {{' ','_',' '},{'|','_','|'},{' ','_','|'}};
    seg_list.push_back(seg_val0);
    seg_list.push_back(seg_val1);
    seg_list.push_back(seg_val2);
    seg_list.push_back(seg_val3);
    seg_list.push_back(seg_val4);
    seg_list.push_back(seg_val5);
    seg_list.push_back(seg_val6);
    seg_list.push_back(seg_val7);
    seg_list.push_back(seg_val8);
    seg_list.push_back(seg_val9);
                
    string res="";
    for(int ind=0; ind<arr[0].size()/arr.size(); ind++){
        res += to_string(find_number(arr, ind, seg_list, 0));
    }
    cout << res << endl;
}

int find_number(vector<string> arr, int index, vector<vector<string>> seg_list, int choice){
    int cpt = 0;
    
    if(choice == 10){ /* the number is not found */
        return -1;
    }

    for(int i=0; i<3; i++){ /* scan each seg of a number, if not matching, pass to an another number */
        for(int j=0; j<3; j++){
            if(arr[i][index*3+j] == seg_list[choice][i][j]){
                cpt++;
            }
            else{
                choice++;
                return find_number(arr, index, seg_list, choice);
            }
        }
    }
    if(cpt == 9){ /* the number is found */
        return choice;
    }
}
