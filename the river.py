import sys
import math

def digit_sum(n):
    res=0
    for c in str(n):
        res += int(c)
    return res

r_1 = int(input())
r_2 = int(input())

r_1, r_2 = min(r_1,r_2), max(r_1,r_2)

while(1):
    r_1 = r_1 + digit_sum(r_1)
    if(r_1 > r_2):
        tmp = r_1
        r_1 = r_2
        r_2 = tmp
    if(r_1 == r_2):
        print(r_1)
        break
