#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

vector<string> all_telephone;

typedef struct Node{
    int val;
    Node *next[10];
}Node;

Node* create_new_node(int val);

static int counter = 0; // number of nodes created

int main()
{
    int N;
    cin >> N; cin.ignore();
    for (int i = 0; i < N; i++) {
        string telephone;
        cin >> telephone; cin.ignore();
        all_telephone.emplace_back(telephone);
    }
    /*for(auto x : all_telephone){
        cerr << x << endl;
    }*/
    Node *root = create_new_node(-1);
    Node *current = create_new_node(-1);
    current = root;
    for(int i=0; i<all_telephone.size(); i++){
        for(int j=0; j<all_telephone[i].size(); j++){
            int node_val = (int)all_telephone[i][j]-48;
            if(current->next[node_val] == NULL){
                current->next[node_val] = create_new_node(node_val);
                counter++;
            }
            current = current->next[node_val];
        }
        current = root;
    }
    
    cout << counter << endl;
}

Node* create_new_node(int val){
    Node *new_node = (Node*)malloc(sizeof(Node));
    new_node->val = val;
    for(int i=0; i<10; i++){
        new_node->next[i] = NULL;
    }
    return new_node;
}
