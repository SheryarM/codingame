import sys
import math

n = int(input())  # number of participant
c = int(input())  # gift cost
tot = 0
bud = []          # budget of all participant

for i in range(n):
    b = int(input())
    bud.append(b)
    tot += b
    
bud.sort()  # sort
i = 0

if(tot < c):
    print("IMPOSSIBLE")  # impossible to purchase a gift by inclunding all participant
else:
    while(c > 0):
        avr = c / len(bud)
        if(bud[i] < avr):
            print(bud[i])  # in that case take participant budget
            c -= bud[i]
            bud.pop(0)
        else:
            print(math.floor(avr))  # in that case take the average
            c -= math.floor(avr)
            bud.pop(0)  # remove the participant after his contribution
        i = 0
