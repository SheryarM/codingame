import sys
import math
from queue import Queue

def BFS(node):
    q.put(node)
    while not q.empty():
        node = q.get()
        for i in range(n):
            if(adj_matrix[node][i] == 1 and i in exits):
                return str(node)+str(" ")+str(i)
            if(adj_matrix[node][i] == 1):
                q.put(i)

adj_matrix = []
zeros = []
exits = []
q = Queue()
# n: the total number of nodes in the level, including the gateways
# l: the number of links
# e: the number of exit gateways
n, l, e = [int(i) for i in input().split()]
for i in range(n):
    for j in range(n):
        zeros.append(0)
    adj_matrix.append(zeros)
    zeros = []
for i in range(l):
    # n1: N1 and N2 defines a link between these nodes
    n1, n2 = [int(j) for j in input().split()]
    adj_matrix[n1][n2] = 1
    adj_matrix[n2][n1] = 1
for i in range(e):
    ei = int(input())  # the index of a gateway node
    exits.append(ei)

while True:
    si = int(input())  # the index of the node on which the Skynet agent is positioned this turn
    res = BFS(si)
    print(res)
    q.queue.clear()
